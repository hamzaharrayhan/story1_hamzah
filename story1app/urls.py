from django.urls import path

from . import views

app_name = 'story1app'

urlpatterns = [
    path('', views.index, name='index'),
]